package run

import (
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"studentgit.kata.academy/SLK/geo-task/cache"
	"studentgit.kata.academy/SLK/geo-task/geo"
	curService "studentgit.kata.academy/SLK/geo-task/module/courier/service"
	curStorage "studentgit.kata.academy/SLK/geo-task/module/courier/storage"
	"studentgit.kata.academy/SLK/geo-task/module/courierfacade/controller"
	curFSservice "studentgit.kata.academy/SLK/geo-task/module/courierfacade/service"
	orService "studentgit.kata.academy/SLK/geo-task/module/order/service"
	orStorage "studentgit.kata.academy/SLK/geo-task/module/order/storage"
	"studentgit.kata.academy/SLK/geo-task/router"
	"studentgit.kata.academy/SLK/geo-task/server"
	"studentgit.kata.academy/SLK/geo-task/workers/order"
	"time"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта redis
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	// инициализация клиента redis
	rclient := cache.NewRedisClient(host, port)
	go func() {
		ticker := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-ticker.C:
				log.Println(rclient.Ping(context.Background()))
			}
		}
	}()
	// инициализация контекста с таймаутом
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	// проверка доступности redis
	_, err := rclient.Ping(ctx).Result()
	if err != nil {
		return err
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := orStorage.NewOrderStorage(rclient)
	// инициализация сервиса заказов
	orderService := orService.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := curStorage.NewCourierStorage(rclient)
	// инициализация сервиса курьеров
	courierSevice := curService.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := curFSservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
