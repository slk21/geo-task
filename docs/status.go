package docs

import "studentgit.kata.academy/SLK/geo-task/module/courierfacade/models"

// swagger:route GET /api/status Status statusRequest
// Вывод списка заказов и информации о курьере
// responses:
// 200: statusResponse

// swagger:parameters statusRequest
type statusRequest struct {
}

// swagger:response statusResponse
type statusResponse struct {
	//in:body
	Body models.CourierStatus
}
