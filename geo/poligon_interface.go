package geo

type PolygonChecker interface {
	Contains(point Point) bool // Проверить, находится ли точка внутри полигона
	Allowed() bool             // Разрешено ли входить в полигон
	RandomPoint() Point        // Сгенерировать случайную точку внутри полигона
}
