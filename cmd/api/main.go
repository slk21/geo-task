package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"studentgit.kata.academy/SLK/geo-task/run"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(2)
	}
	// инициализация приложения
	app := run.NewApp()
	// запуск приложения
	err = app.Run()
	// в случае ошибки выводим ее в лог и завершаем работу с кодом 2
	if err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(2)
	}
}
