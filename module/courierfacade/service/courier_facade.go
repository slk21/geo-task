package service

import (
	"context"
	"log"
	curService "studentgit.kata.academy/SLK/geo-task/module/courier/service"
	curM "studentgit.kata.academy/SLK/geo-task/module/courierfacade/models"
	orM "studentgit.kata.academy/SLK/geo-task/module/order/models"
	orService "studentgit.kata.academy/SLK/geo-task/module/order/service"
)

const (
	CourierVisibilityRadius = 2500 // 2.5km
	CourierOrderRadius      = 5
	Unit                    = "m"
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) curM.CourierStatus     // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService curService.Courierer
	orderService   orService.Orderer
}

func NewCourierFacade(courierService curService.Courierer, orderService orService.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println("Ошибка при получении модели курьера: ", err)
		return
	}
	courier, err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Println("Ошибка при получении модели с данными о курьере: ", err)
		return
	}
	radius, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierOrderRadius, Unit)
	if err != nil {
		log.Println("Ошибка получения заказа курьером: ", err)
		return
	}
	if len(radius) == 0 {
		return
	}
	var order orM.Order
	for _, v := range radius {
		if !v.IsDelivered {
			order = v
			break
		}
	}
	courier.Score++
	err = c.orderService.RmOrder(ctx, order)
	if err != nil {
		log.Println("Ошибка при удалении заказа: ", err)
		return
	}
	err = c.courierService.Save(ctx, *courier)
	if err != nil {
		log.Println("Ошибка при сохранении данных курьера: ", err)
		return
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) curM.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return curM.CourierStatus{}
	}
	radius, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, Unit)
	if err != nil {
		log.Println(err)
	}
	out := curM.CourierStatus{
		Courier: *courier,
		Orders:  radius,
	}
	return out
}
