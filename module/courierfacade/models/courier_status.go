package models

import (
	curM "studentgit.kata.academy/SLK/geo-task/module/courier/models"
	orM "studentgit.kata.academy/SLK/geo-task/module/order/models"
)

type CourierStatus struct {
	Courier curM.Courier `json:"courier"`
	Orders  []orM.Order  `json:"orders"`
}
