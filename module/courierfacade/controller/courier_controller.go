package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"studentgit.kata.academy/SLK/geo-task/module/courierfacade/service"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)
	// отправить статус курьера в ответ
	ctx.JSON(200, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	data, ok := m.Data.([]byte)
	if !ok {
		log.Println("Ошибка получения данных из m.Data")
		return
	}
	err = json.Unmarshal(data, &cm)
	if err != nil {
		log.Println("Ошибка сериализации данных CourierMove", err)
		return
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
