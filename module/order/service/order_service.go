package service

import (
	"context"
	"math"
	"math/rand"
	"studentgit.kata.academy/SLK/geo-task/geo"
	"studentgit.kata.academy/SLK/geo-task/module/order/models"
	"studentgit.kata.academy/SLK/geo-task/module/order/storage"
	"time"
)

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (o *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	return o.storage.GetByRadius(ctx, lng, lat, radius, unit)
}

func (o *OrderService) Save(ctx context.Context, order models.Order) error {
	return o.storage.Save(ctx, order, orderMaxAge)
}

func (o *OrderService) GetCount(ctx context.Context) (int, error) {
	return o.storage.GetCount(ctx)
}

func (o *OrderService) RemoveOldOrders(ctx context.Context) error {
	return o.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (o *OrderService) GenerateOrder(ctx context.Context) error {
	id, err := o.storage.GenerateUniqueID(ctx)
	if err != nil {
		return err
	}
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)

	orderPrice := minOrderPrice + rnd.Float64()*(maxOrderPrice-minOrderPrice)
	deliveryPrice := minDeliveryPrice + rnd.Float64()*(maxDeliveryPrice-minDeliveryPrice)
	randomSpot := geo.GetRandomAllowedLocation(o.allowedZone, o.disabledZones)
	order := models.Order{
		ID:            id,
		Price:         math.Round(orderPrice*100) / 100,
		DeliveryPrice: math.Round(deliveryPrice*100) / 100,
		Lng:           randomSpot.Lng,
		Lat:           randomSpot.Lat,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	}
	err = o.Save(ctx, order)
	if err != nil {
		return err
	}
	return nil
}

func (o *OrderService) RmOrder(ctx context.Context, order models.Order) error {
	return o.storage.RemOrder(ctx, order)
}
