package storage

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
	"log"
	"studentgit.kata.academy/SLK/geo-task/module/order/models"
	"time"
)

type OrderStorage struct {
	storage *redis.Client
}

func NewOrderStorage(storage *redis.Client) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	// получить ID всех старых ордеров, которые нужно удалить
	// используя метод ZRangeByScore
	// старые ордеры это те, которые были созданы две минуты назад
	// и более
	max := time.Now().Add(-maxAge).Unix()

	rangeZ := &redis.ZRangeBy{
		Max: fmt.Sprintf("%d", max),
		Min: "0",
	}
	orders, err := o.storage.ZRangeByScore(ctx, "orders", rangeZ).Result()
	if err != nil {
		log.Println("Ошибка при получении ID старых заказов: ", err)
		return err
	}
	// Проверить количество старых ордеров
	// удалить старые ордеры из redis используя метод ZRemRangeByScore где ключ "orders" min "-inf" max "(время создания старого ордера)"
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни
	if len(orders) > 0 {
		result, err1 := o.storage.ZRemRangeByScore(ctx, "orders", "-inf", rangeZ.Max).Result()
		if err1 != nil {
			log.Println("Ошибка удаления старых заказов: ", err1)
			return err1
		}
		log.Printf("Order %v has been removed", result)
	}
	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var data []byte
	var order models.Order
	// получаем ордер из redis по ключу order:ID
	var key = fmt.Sprintf("order:%d", orderID)
	result, err := o.storage.Get(ctx, key).Result()
	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil
	if errors.Is(err, redis.Nil) {
		log.Println("Ошибка проверки наличия(отсутсвия) ордера", err)
		return nil, nil
	}
	if err != nil {
		log.Println("Ошибка проверки наличия(отсутсвия) ордера", err)
		return nil, err
	}
	// десериализуем ордер из json
	data = []byte(result)
	err = json.Unmarshal(data, &order)
	if err != nil {
		log.Println("Ошибка при десериалицазии ордера: ", err)
		return nil, err
	}
	return &order, nil
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte
	// сериализуем ордер в json
	var key = fmt.Sprintf("order:%d", order.ID)
	data, err = json.Marshal(order)
	if err != nil {
		log.Println("Ошибка при сериалицазии ордера: ", err)
		return err
	}
	// сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge
	err = o.storage.Set(ctx, key, data, maxAge).Err()
	if err != nil {
		log.Println("Ошибка при сохранении ордера: ", err)
		return err
	}
	// добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты
	err = o.storage.GeoAdd(ctx, "order_spots", &redis.GeoLocation{
		Name:      key,
		Longitude: order.Lng,
		Latitude:  order.Lat,
	}).Err()
	if err != nil {
		log.Println("Ошибка при добавлении ордер в гео индекс: ", err)
		return err
	}
	// zset сохраняем ордер для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.ZAdd(ctx, "orders", redis.Z{
		Score:  float64(order.CreatedAt.Unix()),
		Member: key,
	}).Err()
	if err != nil {
		log.Println("Ошибка при сохранении ордера для получения количества заказов: ", err)
		return err
	}
	return err
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard(ctx, "orders").Result()
	if err != nil {
		log.Println("Ошибка при получении количества ордеров: ", err)
		return 0, err
	}
	return int(count), nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var orders []models.Order
	var data []byte
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if errors.Is(err, redis.Nil) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	orders = make([]models.Order, 0, len(ordersLocation))
	// проходим по списку ID заказов и получаем данные о заказе
	// получаем данные о заказе по ID из redis по ключу order:ID
	for _, v := range ordersLocation {
		result, errr := o.storage.Get(ctx, v.Name).Result()
		if errr != nil {
			if errors.Is(errr, redis.Nil) {
				log.Println("Ошибка получения данных о заказе по ID", errr)
				continue
			} else {
				log.Println("Ошибка получения данных о заказе по ID", errr)
				continue
			}
		}
		data = []byte(result)
		var order models.Order
		err = json.Unmarshal(data, &order)
		if err != nil {
			log.Println("Ошибка при десериалицазии ордера: ", err)
			continue
		}
		orders = append(orders, order)
	}
	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки

	query := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	return o.storage.GeoRadius(ctx, "order_spots", lng, lat, query).Result()
}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	var err error
	var id int64
	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	id, err = o.storage.Incr(ctx, "order:id").Result()
	if err != nil {
		log.Println("Ошибка генерации уникально ID для ордера: ", err)
		return 0, err
	}
	return id, nil
}

func (o *OrderStorage) RemOrder(ctx context.Context, order models.Order) error {
	minMax := fmt.Sprintf("%d", order.CreatedAt.Unix())
	err := o.storage.ZRemRangeByScore(ctx, "orders", minMax, minMax).Err()
	if err != nil {
		log.Println("Ошибка при удалении ордера: ", err)
		return err
	}
	err = o.storage.Del(ctx, fmt.Sprintf("order:%d", order.ID)).Err()
	if err != nil {
		log.Println("Ошибка при удалении ордера: ", err)
		return err
	}
	return nil
}
