package service

import (
	"context"
	"errors"
	"github.com/redis/go-redis/v9"
	"log"
	"math"
	"studentgit.kata.academy/SLK/geo-task/geo"
	"studentgit.kata.academy/SLK/geo-task/module/courier/models"
	"studentgit.kata.academy/SLK/geo-task/module/courier/storage"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	courier, err := c.courierStorage.GetOne(ctx)
	if errors.Is(err, redis.Nil) {
		courier = &models.Courier{}
		courier.Location.Lat = DefaultCourierLat
		courier.Location.Lng = DefaultCourierLng
		err = nil
	}
	if err != nil {
		log.Println("Ошибка при получении курьера из хранилища: ", err)
		return nil, err
	}
	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	if !c.allowedZone.Contains(geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}) {
		newPoint := c.allowedZone.RandomPoint()
		courier.Location.Lat = newPoint.Lat
		courier.Location.Lng = newPoint.Lng
	}
	// сохраняем новые координаты курьера
	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		log.Println("Ошибка при сохранении координат курьера курьера: ", err)
		return nil, err
	}
	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) (*models.Courier, error) {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	move := 0.001 / math.Pow(2, float64(zoom-14))
	switch direction {
	case DirectionUp:
		courier.Location.Lat += move
	case DirectionDown:
		courier.Location.Lat -= move
	case DirectionLeft:
		courier.Location.Lng -= move
	case DirectionRight:
		courier.Location.Lng += move
	}
	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}
	if !c.allowedZone.Contains(point) {
		newPoint := c.allowedZone.RandomPoint()
		courier.Location.Lat = newPoint.Lat
		courier.Location.Lng = newPoint.Lng
	}
	// далее сохранить изменения в хранилище
	err := c.courierStorage.Save(context.Background(), courier)
	return &courier, err
}

func (c *CourierService) Save(ctx context.Context, courier models.Courier) error {
	return c.courierStorage.Save(ctx, courier)
}
