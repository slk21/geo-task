package service

import (
	"context"
	"studentgit.kata.academy/SLK/geo-task/module/courier/models"
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) (*models.Courier, error)
	Save(ctx context.Context, courier models.Courier) error
}
