package storage

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"log"
	"studentgit.kata.academy/SLK/geo-task/module/courier/models"
)

const courierKey = "courier"

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	data, err := json.Marshal(courier)
	if err != nil {
		log.Println("Ошибка сериализации данных курьера ", err)
		return err
	}
	err = c.storage.Set(ctx, courierKey, data, 0).Err()
	if err != nil {
		log.Println("Ошибка при помещении данных о курьере", err)
		return err
	}
	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var courier models.Courier
	result, err := c.storage.Get(ctx, courierKey).Result()
	if err != nil {
		log.Println("Ошибка получения данных о курьере: ", err)
		return nil, err
	}
	err = json.Unmarshal([]byte(result), &courier)
	if err != nil {
		log.Println("Ошибка десериализации курьера: ", err)
		return nil, err
	}
	return &courier, nil
}
